"use client";

import { CldVideoPlayer } from "next-cloudinary";
import "next-cloudinary/dist/cld-video-player.css";

export default function CloudinaryVideo({ publicId }) {
  return <CldVideoPlayer width="600" height="350" src={publicId} />;
}
