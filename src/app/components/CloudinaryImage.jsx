"use client";

import { CldImage } from "next-cloudinary";

export default function CloudinaryImage({ publicId }) {
  return (
    <CldImage
      width="960"
      height="600"
      src={publicId}
      sizes="100vw"
      alt="Description of my image"
    />
  );
}
