import Form from "@/app/components/Form";
import React from "react";

export default function Page() {
  return (
    <Form
      title="Sign In"
      buttonText="Sign In"
      signText="Don't have an account? "
      signLink="Sign Up"
      signHref="/register"
    />
  );
}
